<?php 
session_start();
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ctqr";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT QR_ID, time_stamp, checkpointName FROM checkpoints";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    $output = "";
    while($row = $result->fetch_assoc()) {
        $output .= "<div style='display:flex;justify-content:space-around;text-align:left'><span>QR::" . $row["QR_ID"]. 
        "</span>  <span> " . $row["checkpointName"]. 
        "</span> <span> " . $row["time_stamp"]. "</span></div><br>";
    }
} else {
    echo "0 results";
}
$conn->close();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>AIRPORT Contact Tracing</title>
  <link rel="stylesheet" href="./assets/css/style.css">
</head>

<body>
  <div class="mainwrapper">
    <div class="bgboxyellow"></div>
    <div class="bgboxgreen">

    </div>
    <div class="bgboxblack"></div>
    <div class="header">
      <h1 class="logo">AIRPORT</h1>
      <div class="help">
        <i></i>
        HELP
      </div>
    </div>
    <!--  <div class="mainbox">-->
    <div class="boxyBox" style="overflow-y:scroll;">
      <div style='display:flex;justify-content:space-around'><strong>QRID</strong>  <strong>LOCATION</strong>  <strong>TIMESTAMP</strong>
      </div>
      <p></p>
      <?php echo $output ?>
      
  </div>
  <!--<div class="goBack">
        <span>
          <</span> <div>Go back</div>
    </div>
  </div>-->
  <!--    </div>-->
  <div class="footer">
    <p class="copyright">All Rights Reserved &copy;</p>
    <div class="contact">
      <i></i>
      Contact Us
    </div>
  </div>

  </div>
</body>

</html>