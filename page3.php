<?php 
session_start();

if(isset($_GET['qr'])){
 $id = $_GET['qr'];
}

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ctqr";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT QR_ID, time_stamp, checkpointName FROM checkpoints WHERE QR_ID='$id' ORDER BY checkpointName DESC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    $output = "";
    while($row = $result->fetch_assoc()) {
      $cName = $row["checkpointName"];
      $tStmp = $row["time_stamp"];
      $sql = "SELECT q.QR_ID, p.passport_ID FROM checkpoints q 
      INNER JOIN qrcode p 
      ON q.QR_ID = p.QR_ID 
      WHERE q.checkpointName = '$cName'  and q.time_stamp BETWEEN ($tStmp - 30) AND ($tStmp + 30)";
      $result2 = $conn->query($sql);
      if ($result2->num_rows > 0) {
        while($row2 = $result2->fetch_array()) {
          $output .= "<div style='display:flex;justify-content:space-around;text-align:left'><span>QR: " . $row2[0]. "</span>  <span>Passport#: " . $row2[1]."</span>  <span>Location: $cName</span></div><br>";
        }
      }else {
        $output = "0 results";
    }
    }
} else {
  $output = "0 results";
}
$conn->close();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AIRPORT Contact Tracing</title>
    <link rel="stylesheet" href="./assets/css/style.css">
</head>
<body>
    <div class="mainwrapper">
        <div class="bgboxyellow"></div>
        <div class="bgboxgreen">

        </div>
        <div class="bgboxblack"></div>
        <div class="header">
            <h1 class="logo">AIRPORT</h1>
            <div class="help">
                <i></i>
                HELP
            </div>
        </div>
      <!--  <div class="mainbox">-->
      <div class="boxyBox" style="overflow-y:scroll;">
     Selecting IDs who were found to be at same locations as suspect within 30 seconds range
     <div style='display:flex;justify-content:space-around;margin-top:20px;margin-bottom:10px;'><strong>QRID</strong>  <strong>PassportID</strong>  <strong>LOCATION</strong>
      </div>
      <div><?php echo $output?></div>
   
    <!--<div class="goBack">
      <span><</span>
      <div>Go back</div>
    </div>
     </div>-->
  </div>
  <!--    </div>-->  
        <div class="footer">
            <p class="copyright">All Rights Reserved &copy;</p>
            <div class="contact">
                <i></i>
                Contact Us
            </div>
        </div>

    </div>
</body>
</html>